from awscrt import io, mqtt, auth, http
from awsiot import mqtt_connection_builder
import time as t
import json


# Define ENDPOINT, CLIENT_ID, PATH_TO_CERT, PATH_TO_KEY, PATH_TO_ROOT, MESSAGE, TOPIC, and RANGE
ENDPOINT = "a1lb2hmy20ja1y-ats.iot.us-west-2.amazonaws.com"
CLIENT_ID = "parkingLock"
PATH_TO_CERT = "certs/013bb7adb8-certificate.pem.crt"

PATH_TO_KEY = "certs/013bb7adb8-private.pem.key"
PATH_TO_ROOT = "certs/root.pem"
MESSAGE = "Hello World"
TOPIC = "device/publish"
SUBTOPIC = "device/subscribe"
RANGE = 20

def handler(topic,payload):
    print(topic,payload)
    

# Spin up resources
def publishToAws(publishMsg):

    event_loop_group = io.EventLoopGroup(1)
    host_resolver = io.DefaultHostResolver(event_loop_group)
    client_bootstrap = io.ClientBootstrap(event_loop_group, host_resolver)
    mqtt_connection = mqtt_connection_builder.mtls_from_path(
                endpoint=ENDPOINT,
                cert_filepath=PATH_TO_CERT,
                pri_key_filepath=PATH_TO_KEY,
                client_bootstrap=client_bootstrap,
                ca_filepath=PATH_TO_ROOT,
                client_id=CLIENT_ID,
                clean_session=False,
                keep_alive_secs=6
                )
    print("Connecting to {} with client ID '{}'...".format(
            ENDPOINT, CLIENT_ID))
# Make the connect() call
    connect_future = mqtt_connection.connect()
# Future.result() waits until a result is available 
    connect_future.result()
    print("Connected!")
# Publish message to server desired number of times.
    print('Begin Publish')
    mqtt_connection.publish(topic=TOPIC, payload=json.dumps(publishMsg), qos=mqtt.QoS.AT_LEAST_ONCE)
    mqtt_connection.subscribe(SUBTOPIC,handler)
    print('Publish End')
    disconnect_future = mqtt_connection.disconnect()
    disconnect_future.result()

publishToAws("HelloWorld")
