import requests
ENDPOINT = "a1lb2hmy20ja1y-ats.iot.us-west-2.amazonaws.com"
PUBLISH_TOPIC = "device/subscribe"
publish_url = 'https://' +ENDPOINT+ ':8443/topics/' + PUBLISH_TOPIC+ '?qos=1'
publish_msg = "HelloWorld"
CERT_PATH   = "certs/013bb7adb8-certificate.pem.crt"
CERT_KEY    = "certs/013bb7adb8-private.pem.key"


# make request
publish = requests.request('POST',
            publish_url,
            data=publish_msg,
            cert=[CERT_PATH, CERT_KEY])

# print results
print("Response status: ", str(publish.status_code))
if publish.status_code == 200:
        print("Response body:", publish.text)