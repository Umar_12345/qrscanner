from flask import Flask,render_template,request
from awscrt import io, mqtt, auth, http
from awsiot import mqtt_connection_builder
import time as t
import json
import requests
from waitress import serve

ENDPOINT = "a1lb2hmy20ja1y-ats.iot.us-west-2.amazonaws.com"
PUBLISH_TOPIC = "device/subscribe"
publish_url = 'https://' +ENDPOINT+ ':8443/topics/' + PUBLISH_TOPIC+ '?qos=1'

CERT_PATH   = "certs/013bb7adb8-certificate.pem.crt"
CERT_KEY    = "certs/013bb7adb8-private.pem.key"



app = Flask(__name__)

deviceInfo = {"deviceID":0,"userName":0,"password":0,"VechileNumber":0,"controlMessage":"IDLE"}



def publishToAws(publishMsg):
    publish = requests.request('POST',
            publish_url,
            data=publishMsg,
            cert=[CERT_PATH, CERT_KEY])

    # print results
    print("Response status: ", str(publish.status_code))
    if publish.status_code == 200:
        print("Response body:", publish.text)





@app.route('/sendDeviceID/<deviceid>',methods=['GET'])
def deviceID(deviceid=None):
    deviceInfo ["deviceID"] = deviceid
    return (deviceid)


@app.route('/login/<deviceID>',methods=['GET'])
def index(deviceID=None):
    return render_template('loginForm.html')
 
@app.route('/triggerIOTcore',methods=['POST'])
def coreModule():
    if request.method == 'POST':
            deviceInfo["userName"] = request.form['username']
            # deviceInfo["deviceID"] = request.form['deviceID']
            deviceInfo["password"] = request.form['password']
            deviceInfo["VechileNumber"] = request.form['VechileNumber']
            deviceInfo["controlMessage"] = "UNLOCK"
            print(deviceInfo)
            triggerIOTcore = request.form
            publishToAws( json.dumps(deviceInfo))
            return render_template('lockSuccess.html',triggerIOTcore = triggerIOTcore)

@app.route('/scanner')
def scannner():
    return render_template('scanner.html')

   
if __name__ == '__main__':
    app.run(host="192.168.137.1",ssl_context="adhoc")
    #  serve(app, host='0.0.0.0', port=5000, url_scheme='https')

# flask run --cert=adhoc -h 0.0.0.0 -p 8000
